import React from "react";
import PropTypes from "prop-types";
import "./Card.scss";

class Card extends React.Component { 
    constructor(props) { 
        super(props);
        this.add_to_fav = this.add_to_fav.bind(this);
        this.del_from_fav = this.del_from_fav.bind(this);

        this.state = {
            added_star: "0"
        }
    }

    add_to_fav(id) { 
        this.setState({
            added_star: "1"
        });

        localStorage.setItem(`fav_id${id}`, "1");
        this.props.onFavClick(this.props.id);
    }

    del_from_fav(id) { 
        this.setState({
            added_star: "0"
        });

        // if (localStorage.getItem(`id${id}`)) { 
            localStorage.setItem(`fav_id${id}`, "0");
        // } 
        this.props.favRemove(this.props.id);
    }

    componentDidUpdate() { 
        localStorage.setItem(`fav_id${this.props.id}`, this.state.added_star); 
        // console.log(`fav_id{this.props.id}`, `fav_id${this.props.id}`, this.state.added_star);

    }

    componentDidMount() { 
        // if (localStorage.getItem(`fav_id${this.props.id}`)) { 
        if (localStorage.getItem(`fav_id${this.props.id}`) !== null) { 
            this.setState({
            added_star: localStorage.getItem(`fav_id${this.props.id}`)     
            });
        }
            
        //console.log(`fav_id{this.props.id}`, `fav_id${this.props.id}`, this.state.added_star);
        // }
    }

    render() { 
       return (
           <div className="card" id={this.props.id} style={{ backgroundColor: this.props.color }}>
               <div className="card_favorite">
                   {/* <img className="card_favorite_icon" src="../img/star1_white.png" alt="star" onClick={() => { this.props.onFavClick(this.props.id) }} /> */}                   
                   {/* {!(this.props.starAdded - this.props.id===0) && <img className="card_favorite_icon" src="../img/star1_white.png" alt="star" onClick={() => { this.props.onFavClick(this.props.id) }} />}
                   {(this.props.starAdded - this.props.id===0) && <img className="card_favorite_icon" src="../img/star1_added.png" alt="star" onClick={() => { this.props.onFavClick(this.props.id) }} />} */}
                   {/* {(this.state.added_star==="0" || this.state.added_star===null) && <img className="card_favorite_icon" src="../img/star1_white.png" alt="star" onClick={() => { this.add_to_fav(this.props.id) }} />} */}
                   {(this.state.added_star === "0") && <img className="card_favorite_icon" src="../img/star1_white.png" alt="star" onClick={() => { this.add_to_fav(this.props.id) }} />}
                   {(this.state.added_star === "1") && <img className="card_favorite_icon" src="../img/star1_added.png" alt="star" onClick={() => { this.del_from_fav(this.props.id) }} />}
               </div>
                <img className="card_img" src={this.props.url} alt="#" />
                <h3 className="card_title">{this.props.title}</h3>
                <p className="card_price">{this.props.price} $</p>
                <button className ="card_add" onClick={this.props.onclick}>Add to cart</button>
                {/* <div>{this.props.actions}</div> */}
            </div>
        )
    }
}

Card.propTypes = {
    id: PropTypes.number,
    color: PropTypes.string,
    url: PropTypes.string,
    title: PropTypes.string,
    price: PropTypes.number,
    onclick: PropTypes.func,
    onFavClick: PropTypes.func,
    favRemove: PropTypes.func,
    starAdded: PropTypes.number
}

export default Card;