import React from "react";
import Card from "./Card";
import "./Products.scss";

class Products extends React.Component { 
    // constructor(props) { 
    //     super(props);
    // }

    out() { 
        if (this.props.products.wheels) { 
        return (this.props.products.wheels.map(
                   (card) => { return (<Card    key={card.id}
                                                id={card.id} 
                                                color={card.color} 
                                                url={card.url} 
                                                title={card.title} 
                                                price={card.price}
                                                onclick={this.props.onclick}
                                                onFavClick={this.props.onFavClick}
                                                favRemove={ this.props.favRemove}
                                                starAdded={this.props.starAdded}/>);
            }
               ))
        }
        else return null;
    }

    render() { 
       return (
           <div className="products" >
               { this.out()} 
               {/* {this.props.products.wheels.map(
                   (card) => { return <Card id={card.id} color={card.color} url={card.url} title={card.title} price={card.price} />}
               )}                */}
            </div>
        )
    }
}

export default Products;