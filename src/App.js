import React from "react";
import Products from "./components/Products.js";
import Modal from "./components/Modal.js";
import Footer from "./components/Footer.js";
import './App.scss';
import star from "./img/star.png";
import cart from "./img/cart.png";

class App extends React.Component { 
    constructor(props) { 
      super(props);
      this.showModal = this.showModal.bind(this);
      this.addToCart = this.addToCart.bind(this);
      this.addToFav = this.addToFav.bind(this);
      this.removeFromFav = this.removeFromFav.bind(this);
      this.test = this.test.bind(this);
      this.closeModal1 = this.closeModal1.bind(this);

      this.state = {
        products: [],
        add_to_cart: 0,
        add_to_fav: 0,
        add_to_fav_id: "",
        star_added: 0,
        // star_added: "../img/star1_white.png",
        visible1: false,
        visible2: false,
        shadow: false
      };

      this.actions = <div className="actions">
        <div className="ok" onClick={this.addToCart}>Add to cart</div>
        <div className="cancel" onClick={this.closeModal1}>Cancel</div>
      </div>;

      // this.path = "../img/star1_white.png";
      // this.path_added = "../img/star1_added.png";

    }
  
    showModal() {
      // console.log("left");
      this.setState({
        visible1: true,
        // visible2: false,
        shadow: true
      });
    }

    addToCart () {
      let increm = this.state.add_to_cart;
      this.setState({
        add_to_cart: increm + 1     
      });
      localStorage.setItem("in_cart", this.state.add_to_cart);
      // localStorage.setItem(`cart_id${id}`, true);
      this.closeModal1();
    }
  
    addToFav(id) { 
      let increm = this.state.add_to_fav;
      // let str = this.state.add_to_fav_id;
      // console.log(str);

      // if (str.includes(`-id${id}-`) === false) { 
      //   this.setState({
      //     add_to_fav: increm + 1,
      //     add_to_fav_id: str + `-id${id}-`
      //   });
      //   console.log("this.state.add_to_fav_id", this.state.add_to_fav_id);
      //   localStorage.setItem("in_fav", this.state.add_to_fav);
      //   localStorage.setItem("in_fav_id", this.state.add_to_fav_id);
      // }
      // console.log(`fav_id{id}`, `fav_id${id}`);
      // console.log(localStorage.getItem(`fav_id${id}`));

      // if (!localStorage.getItem(`fav_id${id}`)) { 
        // console.log("in!")
        this.setState({
          add_to_fav: increm + 1,
        });
        localStorage.setItem("in_fav", this.state.add_to_fav);
      // }

      this.setState({
        star_added: id
      });

    }
  
    removeFromFav(id) { 
    let increm = this.state.add_to_fav;

      // if (!localStorage.getItem(`fav_id${id}`)) { 
        
        this.setState({
          add_to_fav: increm - 1,
        });
        localStorage.setItem("in_fav", this.state.add_to_fav);
      // }

      this.setState({
        star_added: id
      });
 
    }
  
    // rightBtn() {
    //   // console.log("right");
    //   this.setState({
    //     visible2: true,
    //     visible1: false,
    //     shadow: true
    //   });
    // }
  
    test(e) {
    // console.log(e.target.id);
    if (e.target.id === "test") { 
      this.setState({
        visible1: false,
        // visible2: false,
        shadow: false
      });
    }
    }
  
  closeModal1() { 
    this.setState({
        visible1: false,
        shadow: false
      });
  }

componentDidUpdate(){
  localStorage.setItem("in_cart", this.state.add_to_cart);  
  localStorage.setItem("in_fav", this.state.add_to_fav);  
  localStorage.setItem("in_fav_id", this.state.add_to_fav_id);  
}

  async componentDidMount() { 
    let request = await fetch("./products.json", {
      // headers: {
      //   'Accept': 'application/json',
      //   'Content-Type': 'application/json',
      // } 
    });
    // console.log(await request.json());
    let response = await request.json();
    this.setState({
      products: response
    });

    //--=-=-=-=-=-=-=-!!!!!!!=-=-=--==-=--==-
    this.setState({
      add_to_cart: Number(localStorage.getItem("in_cart")),
      add_to_fav: Number(localStorage.getItem("in_fav")),
      add_to_fav_id: localStorage.getItem("in_fav_id")
    });

    // console.log(response);
  }
  
    render() { 
      return (
        <div className="App" onClick={this.test} id="test">
            <div className="main_header">
              <div className="main_header_in">
                <div className="main_header_title">
                  <p className="main_header_title_1">WHEELS</p>
                  <p className="main_header_title_2">and</p>
                  <p className="main_header_title_3">RIMS</p>
                  <p className="main_header_title_4">FOR YOUR CAR</p>
                </div>
                <div className="cart">
                <img className="cart_img" src={cart} alt="#" />
                <p className="cart_number ">{this.state.add_to_cart}</p>
              </div>
              <div className="favorite">
                <img className="favorite_img" src={star} alt="#"/>
                <p className="favorite_number ">{this.state.add_to_fav}</p>
              </div>
              </div>
            </div>
          {this.state.shadow && <div className="shadow" onClick={this.test} id="test"></div>}
          {this.state.visible1 && <Modal
            text="Are you sure you want to add this wheel to cart?"
            header="ADD PRODUCT TO CART"
            closeButton={true}
            actions={this.actions}
            closeModal={this.closeModal1}
              id={1} />}
            <Products products={this.state.products} onclick={this.showModal} onFavClick={this.addToFav} favRemove={this.removeFromFav} starAdded={this.state.star_added} />
            <Footer />
        </div>
      );   
    }
}

export default App;

